package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test{
  	public static void main(String[] args){	

  		String exePath = "drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
        WebDriver driver = new ChromeDriver();
        String url = "http://sahitest.com/demo/training/login.htm";

        driver.get(url);
        
        String userNameXpath = "/html/body/center/div/form/table/tbody/tr[1]/td[2]/input";  
        String passWordXpath = "/html/body/center/div/form/table/tbody/tr[2]/td[2]/input";
        String loginXpath = "/html/body/center/div/form/table/tbody/tr[3]/td[2]/input";
        
        WebElement userName = driver.findElement(By.xpath(userNameXpath));
        WebElement passWord = driver.findElement(By.xpath(passWordXpath));
        WebElement login = driver.findElement(By.xpath(loginXpath));
        
        
        userName.sendKeys("test");
        passWord.sendKeys("secret");
        login.click();
 
        if(url.equals( driver.getCurrentUrl())){
        	System.out.println("login fallo");
        }else{
        	System.out.println("login correcto");
        }
        
  }
}
